  $pwd = (Resolve-Path .\).Path
  $user = $env:USERNAME
  $wsldir = "C:\Users\$user\AppData\Local\wsl\CentOS8"

if ( -not (Test-Path -LiteralPath $wsldir)) {
  try {
         New-Item -Path $wsldir -ItemType Directory -ErrorAction Stop | Out-Null 
      }
  catch {
          Write-Error -Message "Unable to create directory '$wsldir' . Error was: $_" -ErrorAction Stop
          exit 4 
        }
  "Successfully created directory '$wsldir'."
}
  else {
         "Directory already exists"
          exit 2
  }
echo "Copying files to working dir"
if ( -not (Test-Path -LiteralPath $wsldir\CentOS8.exe)) {
         cp .\CentOS8.exe $wsldir
       }
else {
      "CentOS8.exe exists"
       }
if ( -not (Test-Path -LiteralPath $wsldir\setup.ps1)) {
        cp .\setup.ps1 $wsldir
      }
else {
         rm $wsldir\setup.ps1
         cp .\setup.ps1 $wsldir
       }
if ( -not (Test-Path -LiteralPath $wsldir\uninstall.ps1)) {
        cp .\uninstall.ps1 $wsldir
      }
else {
         rm $wsldir\uninstall.ps1
         cp .\uninstall.ps1 $wsldir
       }
if ( -not (Test-Path -LiteralPath $wsldir\rootfs.tar.gz)) {
        cp .\rootfs.tar.gz $wsldir
      }
else {
         rm $wsldir\rootfs.tar.gz
         cp .\rootfs.tar.gz $wsldir
       }
cd $wsldir
echo "Now running the wsl install script"
.\CentOS8.exe 
Start-Sleep 2
echo "Now setting default User"
wsl.exe -d CentOS8 -e /usr/sbin/useradd $user
Start-Sleep 2
wsl.exe -d CentOS8 -e /usr/bin/passwd $user
Start-Sleep 2
echo "adding user to group wheel"
wsl.exe -d CentOS8 -e /usr/sbin/usermod -a -G wheel $user
wsl.exe -d CentOS8 -u root /bin/bash -c "echo $user ALL=\(ALL\) NOPASSWD: ALL >> /etc/sudoers.d/20-$user"
wsl.exe -d CentOS8 -e /bin/ln -s /mnt/c/Users/$user /home/$user/winhome
Start-Sleep 2
echo "Setting default user"
.\CentOS8.exe config --default-user $user
cd $pwd
echo "Install complete"
